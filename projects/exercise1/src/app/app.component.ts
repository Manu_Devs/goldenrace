import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {

  public productForm!: FormGroup;
  public isSubmitted!: boolean;

  constructor(private fb: FormBuilder) {}

  get productName() { return this.productForm.get('productName'); }

  get productPrice() { return this.productForm.get('price'); }

  ngOnInit(): void {
    this.initValues()
    this.initForm();
  }


  /**
   * @name onSubmit
   * @description
   * This method is called when the form is submitted.
   * @memberof AppComponent
   */
  public onSubmit(): void {
    this.isSubmitted = true;
  }


  /**
   * @name resetForm
   * @description
   * This method is called when the form is reset
   * @memberof AppComponent
   */
  public resetForm(): void {
    this.isSubmitted = false;
    this.productForm.markAsUntouched();
    this.productForm.reset();
  }


  /**
   * @name initValues
   * @description
   * Initializes the component properties
   * @private
   * @memberof AppComponent
   */
  private initValues(): void {
    this.isSubmitted = false;
  }


  /**
   * @name initForm
   * @description
   * Initializes the form
   * @private
   * @memberof AppComponent
   */
  private initForm(): void {
    this.productForm = this.fb.group({
      productName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(19)]],
      price: ['', [Validators.required, Validators.min(6), Validators.max(19)]]
    })
  }

}
