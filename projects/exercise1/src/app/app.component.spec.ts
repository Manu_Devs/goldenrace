import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';


describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const formBuilder: FormBuilder = new FormBuilder();
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [{ provide: FormBuilder, useValue: formBuilder }]
    }).compileComponents();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  describe('Given the user start the application', () => {
    it('should create the app', () => {
      expect(component).toBeTruthy();
    });

    it('should initialice the form', () => {
      component.productForm = formBuilder.group({
        productName: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(19)]],
        price: ['', [Validators.required, Validators.min(6), Validators.max(19)]]
      })
      const initValueSpy = spyOn<any>(component, 'initForm');
      fixture.detectChanges();
      expect(initValueSpy).toHaveBeenCalled()
    })

    describe('When the user submit the form', () => {
      describe('And the product name input', () => {
        it('Should show error message when input length is smaller than 5 characters', fakeAsync(() => {
          setInputValue('.qa-product-name', 'dog');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          let minLength = fixture.debugElement.query(By.css('.qa-product-name-min-length')).nativeElement;
          expect(minLength.innerHTML).toEqual('The product name must be a string longer than 5 characters')
        }))
        it('Should show error message when input length is larger than 20 characters', fakeAsync(() => {
          setInputValue('.qa-product-name', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          let minLength = fixture.debugElement.query(By.css('.qa-product-name-max-length')).nativeElement;
          expect(minLength.innerHTML).toEqual('The product name must be a string smaller than 20 characters')
        }))
      })

      describe('And the product price input', () => {
        it('Should show error message when input value is smaller than 5 ', fakeAsync(() => {
          setInputValue('.qa-product-price', '3');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          let maxLength = fixture.debugElement.query(By.css('.qa-product-price-min-value')).nativeElement;
          expect(maxLength.innerHTML).toEqual('The price must be a number greater than 5')
        }))
        it('Should show error message when input value is larger than 20 ', fakeAsync(() => {
          setInputValue('.qa-product-price', '20');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          let maxLength = fixture.debugElement.query(By.css('.qa-product-price-max-value')).nativeElement;
          expect(maxLength.innerHTML).toEqual('The price must be a number smaller than 20')
        }))
      })

      describe('And the form is valid', () => {
        it('Should set isSubmitted property to true', fakeAsync(() => {
          setInputValue('.qa-product-name', 'Product 1');
          setInputValue('.qa-product-price', '15');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          expect(component.isSubmitted).toBeTrue();
        }))
        it('Should show succesfull message on submit', fakeAsync(() => {
          setInputValue('.qa-product-name', 'Product 1');
          setInputValue('.qa-product-price', '15');
          component.onSubmit();
          fixture.detectChanges();
          tick();
          let maxLength = fixture.debugElement.query(By.css('.qa-success')).nativeElement;
          expect(maxLength.innerHTML).toEqual('Congrats! your form was submitted succesfully')
        }))
      })
    })
    describe('When the user clicks the reset button', () => {
      it('Should reset the form', fakeAsync(() => {
        setInputValue('.qa-product-name', 'Product 1');
        setInputValue('.qa-product-price', '15');
        component.onSubmit();
        component.resetForm()
        fixture.detectChanges();
        let productNameValue = fixture.debugElement.query(By.css('.qa-product-name')).nativeElement;
        let productPriceValue = fixture.debugElement.query(By.css('.qa-product-price')).nativeElement;
        tick();
        expect(productNameValue.value && productPriceValue.value).toEqual('');
      }))
      it('Should set isSubmitted property to false', fakeAsync(() => {
        setInputValue('.qa-product-name', 'Product 1');
        setInputValue('.qa-product-price', '15');
        component.onSubmit();
        component.resetForm()
        fixture.detectChanges();
        tick();
        expect(component.isSubmitted).toBeFalse();
      }))
    })


    /**
     * @name setInputValue
     * @description
     * Set a value for an input field
     * @param {string} selector
     * @param {string} value
     */
    function setInputValue(selector: string, value: string) {
      fixture.detectChanges();
      tick();
      let input = fixture.debugElement.query(By.css(selector)).nativeElement;
      input.value = value;
      input.dispatchEvent(new Event('input'));
      tick();
    };
  });
})

