import { inject, TestBed } from '@angular/core/testing';

import { DataService } from './data.service';

describe('DataService', () => {
  let service: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataService);
  });

  describe('Given a service instance', () => {
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
    
    describe('when subscribing to getNumbers', () => {
      it('Should return a value greater or equal 0', (done: DoneFn) => {
        service.getNumbers().subscribe((value: number) => { expect(value).toBeGreaterThanOrEqual(0), done(); });
      });
      it('Should return a value less or equal 10', (done: DoneFn) => {
        service.getNumbers().subscribe((value: number) => { expect(value).toBeLessThanOrEqual(10), done(); });
      });
    })
  })
});


