import { Component, OnDestroy, OnInit } from '@angular/core';
import { timer, interval, Observable, tap, Subscription } from 'rxjs';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy{

  public value!: number;
  private subscription!: Subscription;
  
  constructor(public dataService: DataService) {}

  ngOnInit(): void {
    this.getNumbers();
  }


  /**
   * @name getNumbers
   * @description
   * Get the numbers from service subcription
   * @memberof AppComponent
   */
  getNumbers(): void  {
    this.subscription = this.dataService.getNumbers().subscribe((val:number) => this.value = val);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
