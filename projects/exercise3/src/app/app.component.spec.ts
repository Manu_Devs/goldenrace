import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { DataService } from './data.service';

export class DataServiceStub {
  getNumbers() {
    return of(6);
  }
}

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [{ provide: DataService, useClass: DataServiceStub }]
    }).compileComponents();

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  describe('Given the user start the application', () => {
    it('Should create the app', () => {
      const app = fixture.componentInstance;
      fixture.detectChanges()
      expect(app).toBeTruthy();
    });

    it('Should initialize the subcription', fakeAsync(() => {
      component.ngOnInit();
      fixture.detectChanges();
      expect(component.value).not.toBeNull();
    }));

    it('Should see number 6 on view', fakeAsync(() => {
      component.ngOnInit();
      fixture.detectChanges();
      let number = fixture.debugElement.query(By.css('.qa-number')).nativeElement;
      expect(number.innerHTML).toEqual('6');
    }));


  })

});
