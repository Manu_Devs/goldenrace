import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  constructor(private router: Router) {
  }


  /**
   * @name navigateTo
   * @description
   * Navigate to the specified path
   * @param {string} path
   * @memberof AppComponent
   */
  navigateTo(path: string): void {
    this.router.navigate([path]);
  }


}
