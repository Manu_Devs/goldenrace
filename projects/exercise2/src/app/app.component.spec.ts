import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth.guard';
import { LazyModule } from './lazy/lazy.module';
import { SecureModule } from './secure/secure.module';
import { Location } from '@angular/common';


describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let location: Location;
  let router: Router;

  const routes: Routes = [
    {
      path: 'lazy',
      loadChildren: () => import('./lazy/lazy.module').then((m) => m.LazyModule)
    },
    {
      path: 'secure',
      loadChildren: () => import('./secure/secure.module').then((m) => m.SecureModule),
      canLoad: [AuthGuard]
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), LazyModule, SecureModule],
      declarations: [AppComponent],
      providers: [AuthGuard]
    }).compileComponents();

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);

    fixture = TestBed.createComponent(AppComponent);
    router.initialNavigation();
  });

  describe('Given user start the application', () => {
    it('should create the app', () => {
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
    describe('When user navigate', () => {
      it('should navigate to the correct route', fakeAsync(() => {
        router = TestBed.inject(Router);
        const app = fixture.componentInstance;
        app.navigateTo('lazy');
        tick();
        expect(location.path()).toEqual('/lazy');
      }))
      it(`navigate to route /lazy load module`, fakeAsync(() => {
        router = TestBed.inject(Router);
        router.navigate(['/lazy']);
        const route = routes[0]
        tick();
        expect(route.loadChildren).not.toBeNull;
        expect(location.path()).toBe('/' + route.path);
      }));

      it(`navigate to route /secure do not load module`, fakeAsync(() => {
        router = TestBed.inject(Router);
        router.navigate(['/secure']);
        const route = routes[1]
        tick();
        expect(route.loadChildren).toBeNull;
        expect(location.path()).toBe('/');
      }));
    })
  })


  
 
});
